コマンドラインで使うにはここが分かりやすかった
http://www.dbonline.jp/sqlite/



プログラムで扱うには
http://www.sqlite.org/download.htmlに行って
「sqlite-amalgamation-3150100.zip」と「sqlite-dll-win32-x86-3150100.zip」をダウンロード

VSで新規プロジェクトを作って
「sqlite3.h」と「sqlite3.def」をプロジェクトフォルダーにコピー
「sqlite3.dll」を実行ファイルと同じ場所（Debugフォルダ）にコピー

VSでメニューの「ツール」⇒「VisualStudioコマンドプロンプト」を選択
lib /def:D:\XXXXX\sqlite3.def /out:D:\XXXXXX\sqlite3.lib　と入力（XXXXXはプロジェクトフォルダのパス）
うまくいけば「sqlite3.lib」ができる


あとはサンプルプログラムを見るべし