#pragma comment( lib, "sqlite3.lib" )

#include <stdio.h>
#include "sqlite3.h"

//抽出結果が返るコールバック関数
static int callback(void *NotUsed, int argc, char **argv, char **azColName){
	int i;
	for (i = 0; i < argc; i++)
		printf("%s = %s  ", azColName[i], argv[i]);
	printf("\n");
	return SQLITE_OK;
}

void main()
{
	
	sqlite3 *db;		//データベース
	char *zErrMsg = 0;	//エラーメッセージが入る


	// データベースファイルを新規生成
	int rc = sqlite3_open("Sample.db", &db);



	// テーブルを生成
	char create_sql[] = "CREATE TABLE sample ( "
		"               id INTEGER PRIMARY KEY, "
		"               name TEXT    NOT NULL,    "
		"               hp INTEGER	NOT NULL)     ";
	rc = sqlite3_exec(db, create_sql, 0, 0, &zErrMsg);



	//データを追加
	rc = sqlite3_exec(db, "INSERT INTO sample ( id, name, hp ) VALUES (0, 'スライム', 20)" , 0, 0, &zErrMsg);
	rc = sqlite3_exec(db, "INSERT INTO sample ( id, name, hp ) VALUES (1, 'ゴブリン', 55)", 0, 0, &zErrMsg);
	rc = sqlite3_exec(db, "INSERT INTO sample ( id, name, hp ) VALUES (2, 'ドラゴン', 999)", 0, 0, &zErrMsg);



	// "sample"テーブルから"nameとhp"を抽出して列挙
	rc = sqlite3_exec(db, "SELECT name , hp FROM sample", callback, 0, &zErrMsg);



	// データベースを閉じる
	sqlite3_close(db);
}